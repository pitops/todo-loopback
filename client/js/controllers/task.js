angular
  .module('app')
  .controller('AddTaskController', ['$scope', 'Task',
      '$state', function($scope, Task, $state) {
    $scope.action = 'Add';
    $scope.task = {};
    $scope.isDisabled = false;

    $scope.submitForm = function() {
      Task
        .create({
          description: $scope.task.description,
        })
        .$promise
        .then(function() {
          $state.go('my-tasks');
        });
    };
  }])
  .controller('DeleteTaskController', ['$scope', 'Task', '$state',
      '$stateParams', function($scope, Task, $state, $stateParams) {
    Task
      .deleteById({ id: $stateParams.id })
      .$promise
      .then(function() {
        $state.go('my-tasks');
      });
  }])
  .controller('EditTaskController', ['$scope', '$q', 'Task',
      '$stateParams', '$state', function($scope, $q, Task,
      $stateParams, $state) {
    $scope.action = 'Edit';
    $scope.task = {};
    $scope.isDisabled = true;

    $q
      .all([
        Task.findById({ id: $stateParams.id }).$promise
      ])
      .then(function(data) {
        $scope.task = data[0];
      });

    $scope.submitForm = function() {
      $scope.task
        .$save()
        .then(function(task) {
          $state.go('my-tasks');
        });
    };
  }])
  .controller('MyTasksController', ['$scope', 'Task', '$rootScope',
      function($scope, Task, $rootScope) {
    $scope.tasks = Task.find({
      filter: {
        where: {
          publisherId: $rootScope.currentUser.id
        },
        include: [
          'tasker'
        ]
      }
    });
  }]);
