var async = require('async');
module.exports = function(app) {
  //data sources
  var mongoDs = app.dataSources.mongoDs;
  //create all models
  async.parallel({
    taskers: async.apply(createTaskers),
  }, function(err, results) {
    if (err) throw err;
    createTasks(results.taskers, function(err) {
      console.log('> models created sucessfully');
    });
  });
  //create reviewers
  function createTaskers(cb) {
    mongoDs.automigrate('Tasker', function(err) {
      if (err) return cb(err);
      var Tasker = app.models.Tasker;
      Tasker.create([
        {email: 'foo@bar.com', password: 'foobar'},
        {email: 'john@doe.com', password: 'johndoe'},
        {email: 'jane@doe.com', password: 'janedoe'}
      ], cb);
    });
  }

  //create tasks
  function createTasks(taskers, cb) {
    mongoDs.automigrate('Task', function(err) {
      if (err) return cb(err);
      var Task = app.models.Task;
      var DAY_IN_MILLISECONDS = 1000 * 60 * 60 * 24;
      Task.create([
        {
          date: Date.now(),
          description: 'Do the laundry',
          publisherId: taskers[0].id
        },
        {
          date: Date.now(),
          description: 'Do my homework',
          publisherId: taskers[1].id
        },
        {
          date: Date.now(),
          description: 'Go to event',
          publisherId: taskers[2].id
        },
      ], cb);
    });
  }
};
